# Radius Manager

Der Radius Manager dient zur vereinfachten Bedienung der Freeradius Server. 


# Files

## .env.example

    ADMIN_USERNAME=admin --> Admin User für den Login
    ADMIN_PASSWORD=passwort123 --> PW für den Login
    LOG_NAME=Log.log --> Name der Log Datei
    LOG_ANZAHL=10 --> Anzahl der Log Einträge auf größen Bildschirmen
    LIMIT_RADIUS_LOG=20 --> Anzahl der Logs pro Seite beim Radius Log
    LOGO_LOGIN=logo_bunt.png --> Logo auf der Login Seite
    LOGO_HEADER=logo_weiss.png --> Logo für den Header sollte am besten weiß sein
    LAN_SERVER=192.168.178.10 --> IP vom 1. Server
    LAN_USER=radius --> Benutzer vom 1. Server
    LAN_PW=Passwort --> PW für den 1. Server
    LAN_DBNAME=radius  --> DB Name auf dem 1. Server
    WLAN_SERVER=192.168.178.11 --> IP vom 2. Server
    WLAN_USER=radius --> Benutzer vom 2. Server
    WLAN_PW=Passwort --> PW für den 2. Server
    WLAN_DBNAME=radius  --> DB Name auf dem 1. Server
    WLAN_VLAN_NAME=Default Management (1),Client (10),Sonoff (5),Shelly (6), Server (4) ---> VLANS 2. Server
    LAN_VLAN_NAME=Default Management (1),Client (10),Shelly (6), Server (4),Kamera (7) --> VLANS 1. Server



# Installation

## Packages

- Apache / NGINX
- PHP 8
- PHP8-MYSQL
- PHP8-CURL

## Configuration

Anpassung der .env datei

    cp ./.env.example ./.env
   Passe danach die Einträge auf deine Wünsche an.

# License
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)    