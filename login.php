<?php
session_start();

// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';

// Benutzername und Passwort für den Admin
$adminUsername = $_ENV['ADMIN_USERNAME'];
$adminPassword = $_ENV['ADMIN_PASSWORD'];

// Überprüfen, ob der Benutzer bereits eingeloggt ist
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    // Benutzer ist bereits eingeloggt, weiter zur create
    include('welcome.php');
} elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Benutzer hat das Login-Formular gesendet

    // Benutzereingaben überprüfen
    $benutzername = $_POST['benutzername'];
    $passwort = $_POST['passwort'];

    if ($benutzername == $adminUsername && $passwort == $adminPassword) {
        // Benutzer erfolgreich eingeloggt
        $_SESSION['loggedin'] = true;

        // Weiter zur welcome.php
        include('welcome.php');
    } else {
        // Falscher Benutzername oder Passwort
        $loginError = true;
        header("Location: loginformular.php?success=0&user=$benutzername"); 
    }
} else {
    // Ansonsten, zeige das Login-Formular
    include('loginformular.php');
}
?>
