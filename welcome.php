<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/header.php';
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radius Manager - Welcome</title>

    <!-- Verwende Tailwind CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #ecf0f1; /* Hellgrau */
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }
        body::before {
            content: "";
            background: url("https://source.unsplash.com/1920x1080/?technology") center center / cover no-repeat;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: -1;
            filter: blur(5px);
        }

        .button-container {
            text-align: center;
            background-color: rgba(255, 255, 255, 0.8);
            padding: 8% 5%;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }
        h1 {
            color: #3498db; /* schönes Blau */
            margin-bottom: 20px; /* Vergrößere den Abstand am unteren Rand der Überschrift */
        }

        .welcome-button {
            margin: 10px;
            font-size: 24px;
            padding: 15px 30px;
            border-radius: 8px;
            background-color: #3498db;
            color: #fff;
            border: none;
            cursor: pointer;
            transition: transform 0.3s ease-in-out;
            display: flex;
            align-items: center; /* Zentriere den Text und das Icon vertikal */
            justify-content: center; /* Zentriere den Text und das Icon horizontal */
        }

        .welcome-button i {
            margin-right: 10px; /* Füge Platz zwischen Text und Icon hinzu */
        }

        .welcome-button:hover {
            transform: scale(1.1);
            color: #fff !important;
            text-decoration: none !important;
            background-color: #2c3e50;
            transition: background-color 0.3s ease-in-out;
        }

        p {
            color: #fff;
            margin-top: 20px;
        }

        p a {
            color: #3498db;
            text-decoration: none;
        }

        p a:hover {
            text-decoration: underline;
        }
        footer {
            text-align: center;
            margin-top: 20px;
            color: #fff;
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #343A40;
            padding: 10px;
        }

        footer a {
            color: #3498db;
            text-decoration: none;
        }

        footer a:hover {
            text-decoration: underline;
        }

        @media (max-width: 640px) {
            .button-container {
                display: flex;
                flex-direction: column;
                align-items: center;
            }
        }
    </style>
</head>
<body>
<div class="button-container">
    <h1 class="text-4xl font-bold mb-4">Radius Manager</h1>
    <br>
    <a href="create-form.php" class="welcome-button"><i class="fas fa-user-plus"></i> User Anlegen</a>
    <a href="delete_user.php" class="welcome-button"><i class="fas fa-user-times"></i> User Löschen</a>
    <a href="radius-log-wlan.php" class="welcome-button"><i class="fas fa-wifi"></i> WLAN Radius Log</a>
    <a href="radius-log-lan.php" class="welcome-button"><i class="fas fa-plug"></i> LAN Radius Log</a>
    <a href="log.php" class="welcome-button"><i class="fas fa-file-alt"></i> Management Log</a>
</div>
<footer>
    <p>&copy; <?php echo date('Y'); ?> - Philipp Hense - <a href="https://it-hense.de">it-hense.de</a></p>
</footer>
</body>
</html>
