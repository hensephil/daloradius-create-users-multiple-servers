<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radius Manager - Login</title>
    <link rel="apple-touch-icon" sizes="180x180" href="./favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon/favicon-16x16.png">
    <link rel="manifest" href="./favicon/site.webmanifest">
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column; /* Ändere die Ausrichtung auf Spaltenrichtung */
            height: 100vh;
            position: relative;
        }

        body::before {
            content: "";
            background: url("https://source.unsplash.com/1920x1080/?technology") center center / cover no-repeat;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            filter: blur(5px); /* Blur-Effekt auf das Hintergrundbild anwenden */
        }

        form {
            background-color: rgba(255, 255, 255, 0.8); /* Farbe und Transparenz für das Milchglas */
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            max-width: 300px;
            width: 100%;
            position: relative;
            z-index: 1; /* Das Formular über dem Pseudo-Element positionieren */
        }

        h1 {
            text-align: center;
            color: #333;
            margin-bottom: 20px; /* Abstand zwischen Überschrift und Logo */
        }

        #logo {
            max-width: 100%; /* Logo auf 100% der verfügbaren Breite begrenzen */
            height: auto; /* Automatische Höhe beibehalten */
            margin-bottom: 20px; /* Abstand zwischen Logo und Benutzernamen */
        }

        label {
            display: block;
            margin-bottom: 8px;
            color: #555;
        }

        input {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        button {
            background-color: #2980b9; /* dunkles Blau für den Button */
            color: #fff;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            background-color: #2574a9; /* dunkleres Blau im Hover-Zustand */
        }

        p {
            text-align: center;
            color: #555;
            margin-top: 20px;
        }

        .error-popup {
            background-color: #e74c3c; /* Dunkles Rot */
            color: #fff;
            padding: 10px;
            border-radius: 4px;
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
    <form action="login.php" method="post" onsubmit="return">
        <img id="logo" src="./<?php echo $_ENV['LOGO_LOGIN']; ?>" alt="Logo" class="logo">
        <h1>Login</h1>
        <?php
        if (isset($_GET['success']) && $_GET['success'] == 0) {
            echo '<p class="error-popup"><b>Username oder Passwort falsch!</b></p>';
        }
        ?>
        
        <label for="benutzername">Benutzername:</label>
        <input type="text" id="benutzername" name="benutzername" value="<?php if (isset($_GET['success']) && $_GET['success'] == 0) {echo $_GET['user'];}; ?>" <?php if (!isset($_GET['success'])) {echo "autofocus";}; ?> required>

        <label for="passwort">Passwort:</label>
        <input type="password" id="passwort" name="passwort" <?php if (isset($_GET['success']) && $_GET['success'] == 0) {echo "autofocus";}; ?> required>

        <button type="submit">Einloggen</button>
        <p>&copy; <?php echo date("Y"); ?> Philipp Hense - <a href="https://it-hense.de" target="_blank">it-hense.de</a></p>
    </form>
</body>
</html>
