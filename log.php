<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';

// Funktion, um Log-Daten zu lesen
function readLogData($logFile, $pageNumber, $entriesPerPage)
{
    // Überprüfe, ob die Log-Datei existiert
    if (file_exists($logFile)) {
        // Lese den Inhalt der Log-Datei
        $logContent = file_get_contents($logFile);

        // Zerlege die Log-Daten in Zeilen
        $logLines = explode("\n", $logContent);

        // Umgekehrte Liste (neueste zuerst)
        $logLines = array_reverse($logLines);

        // Paginierung
        $start = ($pageNumber - 1) * $entriesPerPage;
        $end = $start + $entriesPerPage;

        // Begrenze die Anzahl der Zeilen für mobile Geräte
        $pagedLog = array_slice($logLines, $start, isMobile() ? 5 : $entriesPerPage);

        return $pagedLog;
    } else {
        return array();
    }
}

// Einstellungen für die Paginierung
$pageNumber = isset($_GET['page']) ? max(1, intval($_GET['page'])) : 1;
$entriesPerPage = $_ENV['LOG_ANZAHL']; // Standardanzahl für PC

// Pfad zur Log-Datei
$logFilePath = $_ENV['LOG_NAME'];

// Lese die Log-Daten für die aktuelle Seite
$pagedLogData = readLogData($logFilePath, $pageNumber, $entriesPerPage);

// Funktion zur Erkennung mobiler Geräte
function isMobile()
{
    return (isset($_SERVER['HTTP_USER_AGENT']) &&
            preg_match('/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone)/', $_SERVER['HTTP_USER_AGENT']));
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radius Manager - Managment Log</title>

    <!-- Füge den Header ein -->
    <?php include('header.php'); ?>

    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #ecf0f1; /* Hellgrau */
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        body::before {
            content: "";
            background: url("https://source.unsplash.com/1920x1080/?technology") center center / cover no-repeat;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: -1; /* Hinter das Formular legen */
            filter: blur(5px); /* Blur-Effekt auf das Hintergrundbild anwenden */
        }
        h1 {
            color: #3498db; /* schönes Blau */
            text-align: center; /* Zentriere den Titel */
            font-size: 24px;
            margin-top: 40px; /* Erhöhe den Abstand zum oberen Rand */
        }

        pre {
            white-space: pre-wrap; /* Behandle Zeilenumbrüche in den Log-Daten */
            font-size: 14px; /* Kleinerer Text für bessere Lesbarkeit */
            line-height: 1.5; /* Zeilenabstand für bessere Lesbarkeit */
            background-color: #fff; /* Weißer Hintergrund für bessere Lesbarkeit */
            padding: 10px; /* Innenabstand für bessere Lesbarkeit */
            border-radius: 5px; /* Abgerundete Ecken für bessere Optik */
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); /* Schatten für bessere Trennung vom Hintergrund */
            overflow-x: hidden; /* Füge eine horizontale Scrollbar hinzu, falls der Text zu breit ist */
        }

        p {
            color: #fff;
            margin-top: 20px;
        }

        p a {
            color: #3498db; /* schönes Blau */
            text-decoration: none;
        }

        p a:hover {
            text-decoration: underline;
        }
        footer {
            text-align: center;
            margin-top: 20px;
            color: #fff;
            position: fixed; /* Den Footer am unteren Bildschirmrand positionieren */
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #343A40; /* Hintergrundfarbe für bessere Lesbarkeit */
            padding: 10px;
        }

        footer a {
            color: #3498db; /* schönes Blau */
            text-decoration: none;
        }

        footer a:hover {
            text-decoration: underline;
        }
        form {
            background-color: rgba(255, 255, 255, 0);
            padding: auto;
            border-radius: 5px;
            width: 1100px;
            text-align: center;
            position: relative;
            z-index: 1;
            max-width: 100%;
            overflow-x: hidden; /* Verhindere seitliches Scrollen auf kleinen Bildschirmen */
        }
    </style>
</head>
<body>
    <form>
        <pre>
        <h1>Radius Log</h1>
            <?php
            // Gib die Log-Daten für die aktuelle Seite aus
            foreach ($pagedLogData as $line) {
                echo htmlspecialchars($line) . "\n";
            }
            ?>
            <br>
            <br>
            <?php
            // Erstelle Links für vorherige und nächste Seiten
            $prevPage = $pageNumber - 1;
            $nextPage = $pageNumber + 1;

            echo '<p>';
            if ($prevPage > 0) {
                echo '<a href="?page=' . $prevPage . '">Vorherige Seite</a>';
            }

            if ($prevPage > 0 and count($pagedLogData) == $entriesPerPage) {
                echo ' | ';
            }
            // Nur den "Nächste Seite"-Button anzeigen, wenn es eine nächste Seite gibt
            if (count($pagedLogData) == $entriesPerPage) {
                echo '<a href="?page=' . $nextPage . '">Nächste Seite</a>';
            }
            echo '</p>';
            ?>
        </pre>
    </form>

    <footer>
        <p>&copy; <?php echo date('Y'); ?> - Philipp Hense - <a href="https://it-hense.de">it-hense.de</a></p>
    </footer>
</body>
</html>
