<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';
?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

  <link rel="apple-touch-icon" sizes="180x180" href="./favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="./favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="./favicon/favicon-16x16.png">
  <link rel="manifest" href="./favicon/site.webmanifest">

  <style>
    .header-position {
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      z-index: 1000;
    }
    .navbar-dark .navbar-nav .nav-link {
      color: white;
      margin-right: 20px;
      font-size: 18px;
    }
  </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark header-position">
  <a class="navbar-brand" href="./">
    <img src="./<?php echo $_ENV['LOGO_HEADER']; ?>" width="50" height="50" alt="Logo">
  </a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav justify-content-center w-100">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="managementDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-cogs"></i> Management
        </a>
        <div class="dropdown-menu" aria-labelledby="managementDropdown">
          <a class="dropdown-item" href="create-form.php"><i class="fa fa-user-plus"></i> User Erstellen</a>
          <a class="dropdown-item" href="delete_user.php"><i class="fa fa-user-times"></i> User Löschen</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="radiusLogDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-calendar"></i> Radius Log
        </a>
        <div class="dropdown-menu" aria-labelledby="radiusLogDropdown">
          <a class="dropdown-item" href="radius-log-wlan.php"><i class="fa fa-wifi"></i> WLAN</a>
          <a class="dropdown-item" href="radius-log-lan.php"><i class="fa fa-plug"></i> LAN</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="log.php"><i class="fas fa-file-alt"></i> Log</a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <button class="btn btn-outline-light" onclick="logout()"><i class="fas fa-sign-out-alt"></i> Logout</button>
      </li>
    </ul>
  </div>
</nav>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.7/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
  function logout() {
    window.location.href = 'logout.php';
  }
</script>

</body>
</html>
