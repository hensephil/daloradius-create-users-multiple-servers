<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radius Manager - User löschen</title>

    <!-- Füge den Header ein -->
    <?php include('header.php'); ?>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #ecf0f1; /* Hellgrau */
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        body::before {
            content: "";
            background: url("https://source.unsplash.com/1920x1080/?technology") center center / cover no-repeat;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: -1; /* Hinter das Formular legen */
            filter: blur(5px); /* Blur-Effekt auf das Hintergrundbild anwenden */
        }

        form {
            background-color: rgba(255, 255, 255, 0.8);
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2); /* Leichter Schatten */
            max-width: 500px;
            width: 100%;
            text-align: center;
            position: relative;
            z-index: 1;
        }

        h1 {
            color: #3498db; /* schönes Blau */
        }

        label {
            display: block;
            margin-bottom: 8px;
            color: #555;
        }

        input,
        select {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        button {
            background-color: #3498db; /* schönes Blau */
            color: #fff;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            background-color: #2980b9; /* dunkleres Blau im Hover-Zustand */
        }

        p {
            color: #555;
            margin-top: 20px;
        }

        p a {
            color: #3498db; /* schönes Blau */
            text-decoration: none;
        }

        p a:hover {
            text-decoration: underline;
        }

        /* Stil für Meldungen */
        .success-message {
            background-color: #007533; /* Dunkles Rot */
            color: #fff;
            font-weight: bold;
            padding: 10px;
            border-radius: 4px;
            margin-bottom: 10px;
        }

        .error-message {
            background-color: #e74c3c; /* Dunkles Rot */
            color: #fff;
            font-weight: bold;
            padding: 10px;
            border-radius: 4px;
            margin-bottom: 10px;
        }

        /* Stil für die Fußzeile */
        /* body {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            min-height: 100vh;
            margin: 0;
        } */

        p {
            color: #fff;
            margin-top: 20px;
        }

        p a {
            color: #3498db; /* schönes Blau */
            text-decoration: none;
        }

        p a:hover {
            text-decoration: underline;
        }
        footer {
            text-align: center;
            margin-top: 20px;
            color: #fff;
            position: fixed; /* Den Footer am unteren Bildschirmrand positionieren */
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #343A40; /* Hintergrundfarbe für bessere Lesbarkeit */
            padding: 10px;
        }

        footer a {
            color: #3498db; /* schönes Blau */
            text-decoration: none;
        }

        footer a:hover {
            text-decoration: underline;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>
<body>


    <form action="delete.php" method="post" onsubmit="return validateForm()">
        <h1>Radius - User löschen</h1>

        <?php
        // Überprüfe, ob eine Erfolgsmeldung vorhanden ist
        if (isset($_GET['success']) && $_GET['success'] == 1) {
            echo '<p class="success-message">User erfolgreich gelöscht!</p>';
        }elseif (isset($_GET['success']) && $_GET['success'] == 2) {
            echo '<p class="error-message">Fehler beim Löschen des Users (SQL-Fehler)!</p>';
            $MAC_Value = $_GET['mac'];
            $server_Value = $_GET['server'];
        }
        ?>

        <label for="macAdresse">MAC-Adresse:</label>
        <input type="text" id="macAdresse" name="macAdresse" placeholder="XX:XX:XX:XX:XX:XX" value="<?php if (isset($_GET['success']) && $_GET['success'] > 1) {echo "$MAC_Value";} else {echo NULL;}; ?>" >

        <label for="radius">Radius:</label>
        <select id="radius" name="radius">
            <option value=NULL></option>
            <option value="LAN">LAN</option>
            <option value="WLAN">WLAN</option>
            <option value="Beide">Beide</option>
        </select>

        <button type="submit">Löschen</button>
    </form>

    

    <script>
        function validateForm() {
            var macAdresse = document.getElementById('macAdresse').value;
            var radius = document.getElementById('radius').value;

            if (macAdresse === '' || radius === '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Fehler - Pflichtfelder',
                    text: 'Bitte fülle alle Pflichtfelder aus.',
                });
                return false;
            }

            // Überprüfe das Format der MAC-Adresse
            var macRegex = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/;
            if (!macRegex.test(macAdresse)) {
                Swal.fire({
                    icon: 'error',
                    title: 'Fehler - Falschers Format',
                    text: 'Die MAC-Adresse hat ein ungültiges Format. Bitte verwende das Format XX:XX:XX:XX:XX:XX',
                });
                return false;
            }

            return true;
        }
    </script>
    <footer>
        <p>&copy; <?php echo date('Y'); ?> - Philipp Hense - <a href="https://it-hense.de">it-hense.de</a></p>
    </footer>
    </form>
</body>
</html>