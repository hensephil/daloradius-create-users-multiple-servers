<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';


    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Benutzereingaben abrufen und formatieren
        if (isset($_POST['macAdresse']) && isset($_POST['hostname']) && isset($_POST['radius'])) {
            // Verarbeite die eingegebenen Daten
            $macAdresse = $_POST['macAdresse'];
            $hostname = $_POST['hostname'];
            $radius = $_POST['radius'];
            $VLAN = $_POST['vlanDropdown'];
            $VLAN = trim(preg_replace('/\(\d+\)/', '', $VLAN));
            
            // Log schreiben
            $timestamp = time();
            $datum = date("d.m.Y - H:i:s", $timestamp);
            
            

            // Verbindung zur Datenbank herstellen
            $servernameLAN = $_ENV['LAN_SERVER'];
            $usernameLAN = $_ENV['LAN_USER'];
            $passwordLAN = $_ENV['LAN_PW'];
            $dbnameLAN = $_ENV['LAN_DBNAME'];

            $servernameWLAN = $_ENV['WLAN_SERVER'];
            $usernameWLAN = $_ENV['WLAN_USER'];
            $passwordWLAN = $_ENV['WLAN_PW'];
            $dbnameWLAN = $_ENV['WLAN_DBNAME'];

            // Je nach Radius die Verbindungsinformationen auswählen
            if ($radius == "LAN") {
                $conn = new mysqli($servernameLAN, $usernameLAN, $passwordLAN, $dbnameLAN);
                $macAdresse = strtoupper(str_replace(':', '', $macAdresse));
            } elseif ($radius == "WLAN") {
                $conn = new mysqli($servernameWLAN, $usernameWLAN, $passwordWLAN, $dbnameWLAN);
                $macAdresse = strtolower($macAdresse);
            } else {
                die("Ungültiger Radius");
            }

            // Überprüfen, ob die Verbindung erfolgreich war
            if ($conn->connect_error) {
                die("Verbindung fehlgeschlagen: " . $conn->connect_error);
            }

            // SQL-Query zum Einfügen der Daten
            $timestamp = time();
            $datum = date("Y-m-d H:i:s", $timestamp);
            $Admin = "Radius-Manager-" . $_ENV['ADMIN_USERNAME'];

            $sql0 = "Select username from userinfo";
            $sql1 = "INSERT INTO userinfo (username, firstname, creationby, creationdate) VALUES ('$macAdresse', '$hostname', '$Admin', now())";
            $sql2 = "INSERT INTO radcheck (username, attribute, op, value) VALUES ('$macAdresse', 'Cleartext-Password', ':=', '$macAdresse')";
            $sql3 = "INSERT INTO radusergroup (username, groupname, priority) VALUES ('$macAdresse', '$VLAN', 0)";
            // Query ausführen
            
            $query_users = $conn->query($sql0);
            $users = $query_users->fetch_all(MYSQLI_ASSOC);
            $Count = 0;

            foreach ($users as $user) {
                if ($user["username"] == $macAdresse){
                    $Count = $Count + 1;
                }
            }

            if ($Count == 0){
                if ($conn->query($sql1) === TRUE && $conn->query($sql2) === TRUE && $conn->query($sql3) === TRUE) {
                    $daten = "$datum - Create - MAC: $macAdresse, Hostname: $hostname, VLAN: $VLAN, Server: $radius, Rückgabe: Wurde erfolgreich erstellt\n";
                    file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
                    // Erfolgsmeldung und Rückkehr zur create-form.php
                    $MAC = $_POST['macAdresse'];
                    $VLAN_org = $_POST['vlanDropdown'];
                    header("Location: create-form.php?success=1&mac=$MAC&hostname=$hostname&server=$radius&vlan=$VLAN_org");
                    exit();
                } else {
                    $daten = "$datum - Create - MAC: $macAdresse, Hostname: $hostname, VLAN: $VLAN, Server: $radius, Rückgabe: Fehler beim erstellen\n";
                    file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
                    // Erfolgsmeldung und Rückkehr zur create-form.php
                    $MAC = $_POST['macAdresse'];
                    $VLAN_org = $_POST['vlanDropdown'];
                    header("Location: create-form.php?success=3&mac=$MAC&hostname=$hostname&server=$radius&vlan=$VLAN_org");
                }
            } else {
                $daten = "$datum - Create - MAC: $macAdresse, Hostname: $hostname, VLAN: $VLAN, Server: $radius, Rückgabe: User exestiert bereits\n";
                file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
                $MAC = $_POST['macAdresse'];
                $VLAN_org = $_POST['vlanDropdown'];
                header("Location: create-form.php?success=2&mac=$MAC&hostname=$hostname&server=$radius&vlan=$VLAN_org"); 
            }

            // Verbindung schließen
            $conn->close();
        }
    }
?>