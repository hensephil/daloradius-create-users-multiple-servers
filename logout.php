<?php
session_start();

// Sitzung beenden
session_unset();
session_destroy();

// Weiterleitung zum Login
header("Location: login.php");
exit;
?>
