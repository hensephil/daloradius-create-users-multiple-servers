<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';


    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Benutzereingaben abrufen und formatieren
        if (isset($_POST['macAdresse']) && isset($_POST['radius'])) {
            // Verarbeite die eingegebenen Daten
            $macAdresse = $_POST['macAdresse'];
            $radius = $_POST['radius'];
            
            // Log schreiben
            $timestamp = time();
            $datum = date("d.m.Y - H:i:s", $timestamp);
            
            

            // Verbindung zur Datenbank herstellen
            $servernameLAN = $_ENV['LAN_SERVER'];
            $usernameLAN = $_ENV['LAN_USER'];
            $passwordLAN = $_ENV['LAN_PW'];
            $dbnameLAN = $_ENV['LAN_DBNAME'];

            $servernameWLAN = $_ENV['WLAN_SERVER'];
            $usernameWLAN = $_ENV['WLAN_USER'];
            $passwordWLAN = $_ENV['WLAN_PW'];
            $dbnameWLAN = $_ENV['WLAN_DBNAME'];

            // Je nach Radius die Verbindungsinformationen auswählen
            if ($radius == "LAN") {
                $conn = new mysqli($servernameLAN, $usernameLAN, $passwordLAN, $dbnameLAN);
                $macAdresse = strtoupper(str_replace(':', '', $macAdresse));
                if ($conn->connect_error) {
                    die("Verbindung fehlgeschlagen: " . $conn->connect_error);
                }    
            } elseif ($radius == "WLAN") {
                $conn = new mysqli($servernameWLAN, $usernameWLAN, $passwordWLAN, $dbnameWLAN);
                $macAdresse = strtolower($macAdresse);
                if ($conn->connect_error) {
                    die("Verbindung fehlgeschlagen: " . $conn->connect_error);
                }    
            } elseif ($radius == "Beide") {
                $connWLAN = new mysqli($servernameWLAN, $usernameWLAN, $passwordWLAN, $dbnameWLAN);
                $connLAN = new mysqli($servernameLAN, $usernameLAN, $passwordLAN, $dbnameLAN);
                $macAdresseWLAN = strtolower($macAdresse);
                $macAdresseLAN = strtoupper(str_replace(':', '', $macAdresse));
                if ($connLAN->connect_error) {
                    die("Verbindung fehlgeschlagen: " . $conn->connect_error);
                }
                if ($connWLAN->connect_error) {
                    die("Verbindung fehlgeschlagen: " . $conn->connect_error);
                }
            } else {
                die("Ungültiger Radius");
            }

            // Überprüfen, ob die Verbindung erfolgreich war
            
            // SQL-Query zum Einfügen der Daten
            $timestamp = time();
            $datum = date("Y-m-d H:i:s", $timestamp);
            $Admin = "Radius-Manager-" . $_ENV['ADMIN_USERNAME'];

            // $sql0 = "Select username from userinfo";
            // $sql1 = "INSERT INTO userinfo (username, firstname, creationby, creationdate) VALUES ('$macAdresse', '$hostname', '$Admin', now())";
            // $sql2 = "INSERT INTO radcheck (username, attribute, op, value) VALUES ('$macAdresse', 'Cleartext-Password', ':=', '$macAdresse')";
            // $sql3 = "INSERT INTO radusergroup (username, groupname, priority) VALUES ('$macAdresse', '$VLAN', 0)";

            
            


            // Query ausführen
            
            
            
            if ($radius == "LAN"){
                
                $sql0 = "Select * from userinfo Where username = '$macAdresse'";
                $sql1 = "Delete FROM userinfo WHERE username = '$macAdresse'";
                $sql2 = "Delete FROM radcheck WHERE username = '$macAdresse'";
                $sql3 = "Delete FROM radusergroup WHERE username = '$macAdresse'";

                $query_users = $conn->query($sql0);
                $users = $query_users->fetch_all(MYSQLI_ASSOC);
                foreach ($users as $user) {
                    $hostname = $user['firstname'];
                }
                if ($conn->query($sql1) === TRUE && $conn->query($sql2) === TRUE && $conn->query($sql3) === TRUE) {
                    $daten = "$datum - Delete - MAC: $macAdresse, Hostname: $hostname Server: $radius, Rückgabe: Wurde erfolgreich gelöscht\n";
                    file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
                    // Erfolgsmeldung und Rückkehr zur delete-user.php
                    $MAC = $_POST['macAdresse'];
                    header("Location: delete_user.php?success=1&mac=$MAC&hostname=$hostname&server=$radius");
                    exit();
                } else {
                    // Fehlermeldung und Rückkehr zur delete-user.php
                    $MAC = $_POST['macAdresse'];
                    header("Location: delete_user.php?success=2&mac=$MAC&hostname=$hostname&server=$radius");
                    exit();
                }
            } elseif ($radius == "WLAN"){

                $sql0 = "Select * from userinfo Where username = '$macAdresse'";
                $sql1 = "Delete FROM userinfo WHERE username = '$macAdresse'";
                $sql2 = "Delete FROM radcheck WHERE username = '$macAdresse'";
                $sql3 = "Delete FROM radusergroup WHERE username = '$macAdresse'";

                $query_users = $conn->query($sql0);
                $users = $query_users->fetch_all(MYSQLI_ASSOC);
                foreach ($users as $user) {
                    $hostname = $user['firstname'];
                }
                if ($conn->query($sql1) === TRUE && $conn->query($sql2) === TRUE && $conn->query($sql3) === TRUE) {
                    $daten = "$datum - Delete - MAC: $macAdresse, Hostname: $hostname Server: $radius, Rückgabe: Wurde erfolgreich gelöscht\n";
                    file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
                    // Erfolgsmeldung und Rückkehr zur delete-user.php
                    $MAC = $_POST['macAdresse'];
                    header("Location: delete_user.php?success=1&mac=$MAC&hostname=$hostname&server=$radius");
                    exit();
                } else {
                    // Fehlermeldung und Rückkehr zur delete-user.php
                    $MAC = $_POST['macAdresse'];
                    header("Location: delete_user.php?success=2&mac=$MAC&hostname=$hostname&server=$radius");
                    exit();
                }
            } elseif ($radius == "Beide"){
                $macAdresse = $macAdresseLAN;
                $sql0 = "Select * from userinfo Where username = '$macAdresse'";
                $query_users_LAN = $connLAN->query($sql0);
                $users = $query_users_LAN->fetch_all(MYSQLI_ASSOC);
                foreach ($users as $user) {
                    $hostname_LAN = $user['firstname'];
                }
                $macAdresse = $macAdresseWLAN;
                $sql0 = "Select * from userinfo Where username = '$macAdresse'";
                $query_users_WLAN = $connWLAN->query($sql0);
                $users = $query_users_WLAN->fetch_all(MYSQLI_ASSOC);
                foreach ($users as $user) {
                    $hostname_WLAN = $user['firstname'];
                }
                $Count = 0;
                $macAdresse = $macAdresseWLAN;
                
                $sql1 = "Delete FROM userinfo WHERE username = '$macAdresse'";
                $sql2 = "Delete FROM radcheck WHERE username = '$macAdresse'";
                $sql3 = "Delete FROM radusergroup WHERE username = '$macAdresse'";

                if ($connWLAN->query($sql1) === TRUE && $connWLAN->query($sql2) === TRUE && $connWLAN->query($sql3) === TRUE) {
                    $Count = $Count + 1;
                    $daten = "$datum - Delete - MAC: $macAdresse, Hostname: $hostname_WLAN Server: WLAN, Rückgabe: Wurde erfolgreich gelöscht\n";
                    file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
                }
                $macAdresse = $macAdresseLAN;

                $sql1 = "Delete FROM userinfo WHERE username = '$macAdresse'";
                $sql2 = "Delete FROM radcheck WHERE username = '$macAdresse'";
                $sql3 = "Delete FROM radusergroup WHERE username = '$macAdresse'";

                if ($connLAN->query($sql1) === TRUE && $connLAN->query($sql2) === TRUE && $connLAN->query($sql3) === TRUE) {
                    $Count = $Count + 1;
                    $daten = "$datum - Delete - MAC: $macAdresse, Hostname: $hostname_LAN Server: LAN, Rückgabe: Wurde erfolgreich gelöscht\n";
                    file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
                }
                if ($Count ==2){
                    // Erfolgsmeldung und Rückkehr zur delete-user.php
                    $MAC = $_POST['macAdresse'];
                    header("Location: delete_user.php?success=1&mac=$MAC&hostname=$hostname&server=$radius");
                    exit();
                } else {
                    // Fehlermeldung und Rückkehr zur delete-user.php
                    $MAC = $_POST['macAdresse'];
                    header("Location: delete_user.php?success=2&mac=$MAC&hostname=$hostname&server=$radius");
                    exit();
                }
            }

            // if ($Count == 0){
            //     if ($conn->query($sql1) === TRUE && $conn->query($sql2) === TRUE && $conn->query($sql3) === TRUE) {
            //         $daten = "$datum - Create - MAC: $macAdresse, Hostname: $hostname, VLAN: $VLAN, Server: $radius, Rückgabe: Wurde erfolgreich erstellt\n";
            //         file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
            //         // Erfolgsmeldung und Rückkehr zur create-form.php
            //         $MAC = $_POST['macAdresse'];
            //         $VLAN_org = $_POST['vlanDropdown'];
            //         header("Location: create-form.php?success=1&mac=$MAC&hostname=$hostname&server=$radius&vlan=$VLAN_org");
            //         exit();
            //     } else {
            //         $daten = "$datum - Create - MAC: $macAdresse, Hostname: $hostname, VLAN: $VLAN, Server: $radius, Rückgabe: Fehler beim erstellen\n";
            //         file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
            //         // Erfolgsmeldung und Rückkehr zur create-form.php
            //         $MAC = $_POST['macAdresse'];
            //         $VLAN_org = $_POST['vlanDropdown'];
            //         header("Location: create-form.php?success=3&mac=$MAC&hostname=$hostname&server=$radius&vlan=$VLAN_org");
            //     }
            // } else {
            //     $daten = "$datum - Create - MAC: $macAdresse, Hostname: $hostname, VLAN: $VLAN, Server: $radius, Rückgabe: User exestiert bereits\n";
            //     file_put_contents($_ENV['LOG_NAME'], $daten, FILE_APPEND);
            //     $MAC = $_POST['macAdresse'];
            //     $VLAN_org = $_POST['vlanDropdown'];
            //     header("Location: create-form.php?success=2&mac=$MAC&hostname=$hostname&server=$radius&vlan=$VLAN_org"); 
            // }

            // Verbindung schließen
            $conn->close();
        }
    }
?>