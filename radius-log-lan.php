<?php
// Lade Umgebungsvariablen aus env.php
require_once __DIR__ . '/env.php';
// Füge den Header ein
include('header.php');

// Funktion, um Log-Daten aus der Datenbank abzurufen
function readLogData($mysqli, $tableName, $limit, $offset)
{
    // Abfrage vorbereiten
    $query = "SELECT * FROM $tableName ORDER BY authdate DESC LIMIT $limit OFFSET $offset";
    $result = $mysqli->query($query);

    $logData = array();

    // Überprüfen, ob die Abfrage erfolgreich war
    if ($result) {
        while ($row = $result->fetch_assoc()) {
            $logData[] = $row;
        }
        $result->free();
    }

    return $logData;
}

// MySQLi-Verbindung für WLAN-Server herstellen
$mysqliWLAN = new mysqli($_ENV['LAN_SERVER'], $_ENV['LAN_USER'], $_ENV['LAN_PW'], $_ENV['LAN_DBNAME']);

// Überprüfen, ob die Verbindung erfolgreich war
if ($mysqliWLAN->connect_error) {
    die("Verbindungsfehler (WLAN-Server): " . $mysqliWLAN->connect_error);
}

// Einstellungen für die Pagination
$limit = $_ENV['LIMIT_RADIUS_LOG']; // Anzahl der Einträge pro Seite
$page = isset($_GET['page']) ? max(1, intval($_GET['page'])) : 1;
$offset = ($page - 1) * $limit;

// Log-Daten für WLAN-Server abrufen
$logDataWLAN = readLogData($mysqliWLAN, 'radpostauth', $limit, $offset);
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radius Manager - Log LAN</title>

    <!-- Verwende Tailwind CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #ecf0f1; /* Hellgrau */
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        body::before {
            content: "";
            background: url("https://source.unsplash.com/1920x1080/?technology") center center / cover no-repeat;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: -1; /* Hinter das Formular legen */
            filter: blur(5px); /* Blur-Effekt auf das Hintergrundbild anwenden */
        }
        
        h1 {
            color: #3498db; /* schönes Blau */
        }

        label {
            display: block;
            margin-bottom: 8px;
            color: #555;
        }
        p {
            color: #fff;
            margin-top: 20px;
        }

        p a {
            color: #3498db; /* schönes Blau */
            text-decoration: none;
        }

        p a:hover {
            text-decoration: underline;
        }
        footer {
            text-align: center;
            margin-top: 20px;
            color: #fff;
            position: fixed; /* Den Footer am unteren Bildschirmrand positionieren */
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #343A40; /* Hintergrundfarbe für bessere Lesbarkeit */
            padding: 10px;
        }

        footer a {
            color: #3498db; /* schönes Blau */
            text-decoration: none;
        }

        footer a:hover {
            text-decoration: underline;
        }

        /* Stil für die Erfolgs- und Fehlerzeilen */
        .success-row {
            background-color: #a5d6a7; /* Hellgrün */
        }

        .error-row {
            background-color: #ef9a9a; /* Hellrot */
        }

        /* Stil für den Log-Container */
        #log-container {
            max-height: 60vh;
            overflow-y: auto;
            max-width: 100vh;

        }
        /* Stil für die Tabelle */
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 8px;
            overflow-x: auto; /* Füge horizontales Scrollen hinzu, wenn die Tabelle zu breit ist */
        }

        /* Stil für die Tabellenzellen */
        th, td {
            padding: 8px; /* Verringere den Zellenabstand */
            font-size: 14px; /* Verkleinere den Text standardmäßig */

            /* Media Query für Bildschirmgrößen kleiner als 600px (typischerweise Handys) */
            @media (max-width: 600px) {
                font-size: 11px; /* Reduziere die Schriftgröße für kleinere Bildschirme */
            }
        }
    </style>
</head>
<body class="bg-gray-100 p-4">

<div class="max-w-2xl mx-auto bg-white p-8 rounded shadow-md">
    <h1 class="text-2xl font-bold mb-4">Radius Log - LAN</h1>

    <div id="log-container">
        <table class="w-full border mb-8">
            <thead>
            <tr>
                <th class="border p-2">Auth Date</th>
                <th class="border p-2">Username</th>
                <th class="border p-2">Reply</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($logDataWLAN as $logEntry): ?>
                <?php
                // Prüfe den Reply-Wert und setze die entsprechende Klasse
                $rowClass = '';
                if ($logEntry['reply'] == 'Access-Accept') {
                    $rowClass = 'success-row';
                } elseif ($logEntry['reply'] == 'Access-Reject') {
                    $rowClass = 'error-row';
                }
                ?>
                <tr class="<?php echo $rowClass; ?>">
                    <td class="border p-2"><?php echo htmlspecialchars($logEntry['authdate']); ?></td>
                    <td class="border p-2"><?php echo htmlspecialchars($logEntry['username']); ?></td>
                    <td class="border p-2"><?php echo htmlspecialchars($logEntry['reply']); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <!-- Pagination-Links -->
    <div class="flex justify-between">
        <?php if ($page > 1): ?>
            <a href="?page=<?php echo $page - 1; ?>" class="text-blue-500">&lt; Vorherige Seite</a>
        <?php endif; ?>

        <?php if (!empty($logDataWLAN)): ?>
            <a href="?page=<?php echo $page + 1; ?>" class="text-blue-500">Nächste Seite &gt;</a>
        <?php endif; ?>
    </div>
</div>
<footer>
    <p>&copy; <?php echo date('Y'); ?> - Philipp Hense - <a href="https://it-hense.de">it-hense.de</a></p>
</footer>
</body>
</html>